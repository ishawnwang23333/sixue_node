const app = require('./app');
const port = process.env.PORT || 3000;

const winston = require('winston');
require('winston-loggly-bulk');

winston.add(winston.transports.Loggly, {
    token: "197e22ec-1a9a-4892-961b-ebbdaa32364c",
    subdomain: "ishawnwang",
    tags: ["Winston-NodeJS"],
    json:true
});

const server = app.listen(port, function() {
    winston.log('info','Express server listening on port ' + port);
});

app.use(function(req, res) {
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    res.status(404).send({url: fullUrl + ' not found'})
});