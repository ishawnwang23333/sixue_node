const mongoose = require('mongoose')
    , Schema = mongoose.Schema;

const ChapterVideoSchema = new mongoose.Schema({
    title:String,
    duration:String
});

const ChapterSchema = new mongoose.Schema({
   title:String,
   videos:[ChapterVideoSchema]
});

const AuthorSchema = new mongoose.Schema({
    name:String,
    avatar:String,
    bio:String,
    description:String
});

mongoose.model('Author', AuthorSchema, 'author');

const CourseSchema = new mongoose.Schema({
    title: String,
    subtitle: String,
    description: String,
    score:String,
    releaseDate:String,
    category:[String],
    chapter:[ChapterSchema],
    author:[{ type: Schema.Types.ObjectId, ref: 'Author' }],
    tag:[String],
    suggestedCourses:[String]
});

mongoose.model('Course', CourseSchema, 'course');

module.exports = mongoose.model('Course');
module.exports.AuthorSchema = AuthorSchema;
module.exports.Author = mongoose.model('Author');
