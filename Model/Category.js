const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
    id:String,
    title:String,
    subtitle:String,
    router:String,
    image:String
});

mongoose.model('Category', CategorySchema, 'category');

module.exports = mongoose.model('Category');
