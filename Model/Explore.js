const Constants = require(process.cwd() + '/Constants');
const Category = require(Constants.ROOT + '/Model/Category');

const mongoose = require('mongoose');

const ExploreSchema = new mongoose.Schema({
    title:String,
    type:String,
    data:[Category.schema]
});

mongoose.model('Explore', ExploreSchema,'explore');

module.exports = mongoose.model('Explore');