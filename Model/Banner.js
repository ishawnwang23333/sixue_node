const mongoose = require('mongoose');
const BannerSchema = new mongoose.Schema({
    router: String,
    image: String,
});
mongoose.model('Banner', BannerSchema, 'banner');

module.exports = mongoose.model('Banner');
