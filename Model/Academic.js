const mongoose = require('mongoose');
const AcademicSchema = new mongoose.Schema({
    name:String,
    type:String,
    listen:String,
    learned:String,
    router: String,
    avatar: String,
    price:String,
    tag:[String],
});
mongoose.model('Academic', AcademicSchema, 'academic');

module.exports = mongoose.model('Academic');
