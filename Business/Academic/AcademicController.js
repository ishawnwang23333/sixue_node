const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Constants = require(process.cwd() + '/Constants');
const url = require('url');

router.use(bodyParser.urlencoded({ extended: true }));
const Academic = require(Constants.ROOT + '/Model/Academic');

router.get('/', function (req, res) {
    Academic.find().sort('id').exec(function (err,academic) {
        if (err) return res.status(500).send();
        res.status(200).send(academic);
    });
});

router.get('/:id', function (req, res) {
    Academic.findById(req.params.id).exec(function (err,academic) {
        if (err) return res.status(500).send();
        if (!academic) return res.status(404).send();
        res.status(200).send(academic);
    });
});

// router.delete('/:id', function (req, res) {
//     User.findByIdAndRemove(req.params.id, function (err, user) {
//         if (err) return res.status(500).send("There was a problem deleting the user.");
//         res.status(200).send("User: "+ user.name +" was deleted.");
//     });
// });


module.exports = router;
