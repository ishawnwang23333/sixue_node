const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Constants = require(process.cwd() + '/Constants');
const url = require('url');

router.use(bodyParser.urlencoded({ extended: true }));
const Category = require(Constants.ROOT + '/Model/Category');

router.get('/', function (req, res) {
    Category.find().sort('id').exec(function (err,category) {
        if (err) return res.status(500).send();
        res.status(200).send(category);
    });
});

router.get('/:id', function (req, res) {
    Category.findById(req.params.id).exec(function (err,category) {
        if (err) return res.status(500).send();
        if (!category) return res.status(404).send();
        res.status(200).send(category);
    });
});

router.put('/:id', function (req, res) {
    Category.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, user) {
        if (err) return res.status(500).send(JSON.stringify({'error':'error!'}));
        res.status(200).send(user);
    });
});

router.post('/', function (req, res) {
    Category.create({
            title : req.body.title,
            subtitle : req.body.subtitle,
            router : req.body.router,
            image : req.body.image
        },
        function (err, category) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(category);
        });
});

// router.delete('/:id', function (req, res) {
//     User.findByIdAndRemove(req.params.id, function (err, user) {
//         if (err) return res.status(500).send("There was a problem deleting the user.");
//         res.status(200).send("User: "+ user.name +" was deleted.");
//     });
// });


module.exports = router;
