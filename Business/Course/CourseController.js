const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Constants = require(process.cwd() + '/Constants');
const url = require('url');

router.use(bodyParser.urlencoded({ extended: true }));
const Course = require(Constants.ROOT + '/Model/Course');
const Author = require(Constants.ROOT + '/Model/Course').Author;


router.get('/', function (req, res) {
    Course.find().populate('author').exec(function (err,courses) {
        if (err) return res.status(500).send();
        res.status(200).send(courses);
    });

});

router.get('/:id', function (req, res) {

    Course.findById(req.params.id).populate('author').exec(function (err,courses) {
        if (err) return res.status(500).send();
        if (!courses) return res.status(404).send();
        res.status(200).send(courses);
    });
});

module.exports = router;

