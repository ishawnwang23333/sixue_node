const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const Constants = require(process.cwd() + '/Constants');
const url = require('url');

router.use(bodyParser.urlencoded({ extended: true }));
const Banner = require(Constants.ROOT + '/Model/Banner');
const Explore = require(Constants.ROOT + '/Model/Explore');


router.get('/', function (req, res) {

    Explore.find().sort('id').exec(function (err,explores) {
        Banner.find({},function (err,banners) {
            if (err) return res.status(500).send();
            res.status(200).send({"banner":banners,"explore":explores});
        });
    });
});

module.exports = router;
