'use strict';

const winston = require('winston');
const mongoose = require('mongoose');
const Constants = require(process.cwd() + '/Constants');
const Course = require(Constants.ROOT + '/Model/Course');

mongoose.connect('mongodb://iShawnWang:ws19940415@localhost:27017/sixue?authSource=admin');
mongoose.set('debug', true);
const db = mongoose.connection;
db.on('error', function (error) {
    winston.log('db', 'connection error:');
});

db.once('open', function() {
    winston.log('db',"Mongo DB connected ~");
});

mongoose.set('debug', function (coll, method, query, doc) {
    winston.log('db','Mongoose executed:', coll, method, query, doc);
});