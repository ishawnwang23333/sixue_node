'use strict';

const express = require('express');
const cors = require('cors');
const db = require('./DB/db');
const app = express();

app.use(cors());

const ExploreController = require('./Business/Explore/ExploreController');
const CategoryController = require('./Business/Category/CategoryController');
const CourseController = require('./Business/Course/CourseController');
const AcademicController = require('./Business/Academic/AcademicController');



app.use('/explore', ExploreController);
app.use('/category', CategoryController);
app.use('/course', CourseController);
app.use('/academic', AcademicController);


module.exports = app;

